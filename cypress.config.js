const { defineConfig } = require("cypress");
const getspecFiles = require("cypress-gitlab-parallel-runner")

module.exports = defineConfig({
  
  e2e: {
    setupNodeEvents(on, config) {
      getspecFiles("cypress/e2e/2-advanced-examples",true)
    },
  },
});